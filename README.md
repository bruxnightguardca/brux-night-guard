Brux Night Guard was created by teeth grinders to help other teeth grinders find solutions to Bruxism and sleep problems including teeth grinding and clenching, TMJ, and jaw pain.

We are devoted to educating people about the benefits of a good night's sleep, and ways to promote overall general health. Sleep health leads to good health along with proper diet and exercise.